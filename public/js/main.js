
$(document).on('click', '.btn_delete_doctor', function (e, cal = true) {
    if (cal) {
        e.preventDefault();
        let name = $(this).data('name');
        let id = $(this).data('id');
        Swal.fire({
            title: `<strong>¿Seguro que quieres eliminar al médico ${name}?</strong>`,
            icon: 'info',
            html:
                'Si eliminas al médico seleccionado todas sus citas tambien serán eliminadas',
            reverseButtons: true,
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            cancelButtonText: 'Cancelar',
            cancelButtonAriaLabel: 'Thumbs down',
            confirmButtonText: 'Eliminar',
            confirmButtonAriaLabel: 'Thumbs up, great!',
            confirmButtonColor: '#f5365c',
            cancelButtonColor: '#5e72e4',
        }).then(
            function (isConfirm) {
                if (isConfirm.value) {
                    $(".btn_delete_doctor").trigger(e.type, false);
                }
            },
        );
    } 
})

$(document).on('click', '.btn_delete_speciality', function (e, cal = true) {
    if (cal) {
        e.preventDefault();
        let name = $(this).data('name');
        let id = $(this).data('id');
        Swal.fire({
            title: `<strong>¿Seguro que quieres eliminar la especialidad ${name}?</strong>`,
            icon: 'info',
            html:
                'Si eliminas la especialidad seleccionada todas sus citas asociadas tambien serán eliminadas',
            reverseButtons: true,
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            cancelButtonText: 'Cancelar',
            cancelButtonAriaLabel: 'Thumbs down',
            confirmButtonText: 'Eliminar',
            confirmButtonAriaLabel: 'Thumbs up, great!',
            confirmButtonColor: '#f5365c',
            cancelButtonColor: '#5e72e4',
        }).then(
            function (isConfirm) {
                if (isConfirm.value) {
                    $(".btn_delete_speciality").trigger(e.type, false);
                }
            },
        );
    } 
})



$(document).on('click', '.btn_delete_patient', function (e, cal = true) {
    if (cal) {
        e.preventDefault();
        let name = $(this).data('name');
        let id = $(this).data('id');
        Swal.fire({
            title: `<strong>¿Seguro que quieres eliminar al paciente  ${name}?</strong>`,
            icon: 'info',
            html:
                'Si eliminas al paciente seleccionado todas sus citas asociadas tambien serán eliminadas',
            reverseButtons: true,
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            cancelButtonText: 'Cancelar',
            cancelButtonAriaLabel: 'Thumbs down',
            confirmButtonText: 'Eliminar',
            confirmButtonAriaLabel: 'Thumbs up, great!',
            confirmButtonColor: '#f5365c',
            cancelButtonColor: '#5e72e4',
        }).then(
            function (isConfirm) {
                if (isConfirm.value) {
                    $(".btn_delete_patient").trigger(e.type, false);
                }
            },
        );
    } 
})