@extends('layouts.panel')

@section('content')
<style>
  .turnos-foot {
    width: 100%;
    overflow: hidden;
    height: 47px;
    position: relative;
    background: rgb(1, 6, 92);
    background: linear-gradient(90deg, rgba(1, 6, 92, 1) 0%, rgba(9, 19, 166, 1) 89%);
    font-weight: bold;

  }

  .text-animate {
    right: 0px;
    top: 50%;
    transform: translate(100%, -50%);
    position: absolute;
    color: #fff;
    animation: texto 10s infinite linear;

  }

  .text-animate>p {
    font-size: 25px;
  }

  @keyframes texto {
    from {
      transform: translate(100%, -50%);
    }

    to {
      transform: translate(-300%, -50%);
    }
  }
</style>
<div class="row">
  <div class="col-md-12 mb-4">
    <div class="card">


      <div class="card-body p-0 overflow-hidden">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
        </div>
        @endif

        <img class="img-fluid" style="position: relative;top: -26px;" src="{{asset('img/wellcome.gif')}}" alt="">
        <div class="turnos-foot" style="position: relative;top: -26px;">
          <div class="text-animate">
            <p class="m-0 p-0">🚑 ¡Bienvenido {{auth()->user()->name}}! 👩‍⚕️👨‍⚕️❤️‍ </p>
          </div>
        </div>
      </div>
    </div>
  </div>







  @if (auth()->user()->role == 'admin')
  <div class="d-none col-xl-6 mb-5 mb-xl-0">
    <div class="card shadow">
      <div class="card-header bg-transparent">
        <div class="row align-items-center">
          <div class="col">
            <h6 class="text-uppercase ls-1 mb-1">Notificación general</h6>
            <h2 class="mb-0">Enviar a todos los usuarios</h2>
          </div>
        </div>
      </div>
      <div class="card-body">
        @if (session('notification'))
        <div class="alert alert-success" role="alert">
          {{ session('notification') }}
        </div>
        @endif

        <form action="{{ url('/fcm/send') }}" method="post">
          @csrf
          <div class="form-group">
            <label for="title">Título</label>
            <input value="{{ config('app.name') }}" type="text" class="form-control" name="title" id="title" required>
          </div>
          <div class="form-group">
            <label for="body">Mensaje</label>
            <textarea name="body" id="body" rows="2" class="form-control" required></textarea>
          </div>
          <button class="btn btn-primary">Enviar notificación</button>
        </form>
      </div>
    </div>
  </div>
  <div class="d-none col-xl-6">
    <div class="card shadow">
      <div class="card-header bg-transparent">
        <div class="row align-items-center">
          <div class="col">
            <h6 class="text-uppercase text-muted ls-1 mb-1">Total de citas</h6>
            <h2 class="mb-0">Según día de la semana</h2>
          </div>
        </div>
      </div>
      <div class="card-body">
        <!-- Chart -->
        <div class="chart">
          <canvas id="chart-orders" class="chart-canvas"></canvas>
        </div>
      </div>
    </div>
  </div>
  @endif
</div>
@endsection

@section('scripts')
<script>
  const appointmentsByDay = @json($appointmentsByDay);
</script>
<script src="{{ asset('js/charts/home.js') }}"></script>
@endsection