# CLINDEV

## Guia de implementación

### Levantar base de datos
- levantar servidor mysql wamp, mamp, xamp, laragon ...
- crear base de datos nombre 'my-appointments'
- ejecutar el script ubicado en db/my-appointments.sql

### Levantar proyecto
- cp .env.example .env
- cambiar valores de coneccion a la base el archivo .env generado
- composer update
- php artisan key:generate
- php artisan serve
