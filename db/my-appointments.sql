/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : my-appointments

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 21/09/2021 18:23:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for appointments
-- ----------------------------
DROP TABLE IF EXISTS `appointments`;
CREATE TABLE `appointments`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `specialty_id` int(10) UNSIGNED NOT NULL,
  `doctor_id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(10) UNSIGNED NOT NULL,
  `scheduled_date` date NOT NULL,
  `scheduled_time` time(0) NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Reservada',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `appointments_specialty_id_foreign`(`specialty_id`) USING BTREE,
  INDEX `appointments_doctor_id_foreign`(`doctor_id`) USING BTREE,
  INDEX `appointments_patient_id_foreign`(`patient_id`) USING BTREE,
  CONSTRAINT `appointments_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `appointments_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `appointments_specialty_id_foreign` FOREIGN KEY (`specialty_id`) REFERENCES `specialties` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 301 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of appointments
-- ----------------------------
INSERT INTO `appointments` VALUES (1, 'Amet qui asperiores est dolorem.', 1, 3, 10, '2021-03-21', '01:18:00', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (2, 'Sit beatae et et dicta.', 2, 58, 27, '2021-01-03', '01:16:05', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (3, 'Et eum quos quia dolorum id quia.', 3, 58, 28, '2021-07-11', '17:54:07', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (4, 'Consequatur numquam voluptatem error et.', 1, 59, 40, '2020-10-25', '06:33:47', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (5, 'Suscipit facilis et maiores non suscipit.', 2, 58, 34, '2020-10-08', '04:09:10', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (6, 'Eos maiores ab ex.', 2, 3, 42, '2021-04-16', '07:56:31', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (7, 'At doloremque eaque corporis est libero.', 2, 58, 53, '2021-06-18', '02:47:09', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (8, 'Exercitationem quia enim nobis magnam quos.', 2, 55, 2, '2021-02-03', '04:30:31', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (9, 'Aut voluptatem pariatur at cumque minus.', 3, 3, 19, '2021-06-27', '10:08:04', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (10, 'Voluptas fugit modi aut.', 3, 61, 32, '2021-02-05', '20:17:48', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (11, 'Qui est optio in pariatur perspiciatis omnis.', 1, 54, 16, '2021-07-28', '03:23:21', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (12, 'Quasi culpa et sit ut.', 2, 56, 22, '2021-08-29', '18:03:09', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (13, 'Saepe facilis aperiam dolore.', 3, 62, 16, '2021-07-19', '10:27:34', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (14, 'Provident ut nihil aut quaerat.', 3, 60, 13, '2020-12-02', '09:47:05', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (15, 'Et sit consequatur dolorem voluptatum.', 1, 56, 44, '2020-11-19', '22:57:48', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (16, 'Cum numquam atque itaque qui magnam.', 1, 60, 34, '2021-05-31', '06:50:06', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (17, 'Similique aut ut sapiente magnam accusamus distinctio aliquam.', 3, 62, 18, '2021-08-09', '09:28:53', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (18, 'Sint sapiente nihil facere.', 3, 57, 4, '2020-12-18', '22:05:19', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (19, 'Maxime est asperiores omnis hic rem.', 2, 58, 42, '2020-11-10', '10:54:52', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (20, 'Nihil quidem enim est dolores iste.', 3, 62, 2, '2021-02-08', '11:45:47', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (21, 'Laborum voluptatum veritatis explicabo corporis eum facere.', 2, 62, 45, '2020-09-30', '07:14:50', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (22, 'Maxime sed fuga veniam iste minima.', 2, 54, 45, '2021-09-17', '15:07:38', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (23, 'Error sed voluptas id eos ipsum quis.', 2, 54, 26, '2021-07-03', '17:05:04', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (24, 'Vel aut nisi voluptas voluptas.', 1, 3, 20, '2021-08-24', '22:45:03', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (25, 'Fuga velit voluptas rem omnis eius.', 3, 57, 4, '2021-05-04', '23:47:06', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (26, 'Et soluta laboriosam cumque enim.', 2, 59, 41, '2021-04-11', '02:11:29', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (27, 'Mollitia optio sequi aut sapiente aliquid.', 1, 56, 29, '2020-10-07', '10:38:00', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (28, 'Tempore repellendus sit dolore dolor ipsam.', 2, 61, 32, '2021-08-03', '19:37:17', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (29, 'Eos voluptatibus totam est repellendus unde.', 2, 59, 15, '2021-05-11', '21:06:16', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (30, 'Magnam enim sed et ea.', 2, 55, 21, '2020-10-19', '06:23:33', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (31, 'Reprehenderit quo et cumque.', 2, 59, 21, '2020-11-02', '14:47:07', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (32, 'Et sit voluptas soluta eum voluptatem.', 2, 58, 27, '2021-04-01', '23:46:59', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (33, 'Numquam sint aut odit non accusamus.', 1, 55, 35, '2021-03-29', '10:57:37', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (34, 'Accusantium occaecati voluptas hic dolorem vero.', 1, 3, 31, '2021-06-06', '17:07:59', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (35, 'Ullam rerum est vel.', 3, 3, 11, '2020-11-20', '07:07:49', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (36, 'Occaecati quibusdam quo neque nostrum.', 2, 3, 22, '2021-04-01', '08:54:06', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (37, 'Omnis quo et ut quo.', 3, 61, 12, '2021-03-25', '11:56:08', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (38, 'Id nobis earum itaque sed autem enim.', 1, 3, 46, '2021-04-28', '21:02:54', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (39, 'Provident sapiente qui est at.', 2, 59, 32, '2020-12-06', '18:44:58', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (40, 'Aut officiis sit voluptatibus nam laborum.', 2, 54, 46, '2020-12-18', '16:00:05', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (41, 'Et et libero dolorem reprehenderit similique placeat.', 3, 62, 29, '2021-02-11', '17:40:21', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (42, 'Temporibus voluptatem explicabo expedita aspernatur corrupti aut sed.', 2, 3, 32, '2021-09-17', '19:17:21', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (43, 'Qui ipsam eum ea.', 2, 56, 21, '2021-08-20', '13:09:36', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (44, 'Aliquam expedita labore atque omnis iste.', 1, 58, 9, '2021-08-18', '06:30:29', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (45, 'Quam nostrum minima et molestiae ut voluptas.', 3, 55, 27, '2021-05-19', '20:15:01', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (46, 'Eos ipsa voluptatem aperiam deserunt quae aut.', 3, 3, 4, '2021-08-29', '22:23:42', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (47, 'Deleniti voluptatem cum similique atque.', 2, 3, 28, '2021-03-22', '14:23:12', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (48, 'Debitis illo nisi fugit.', 1, 60, 11, '2021-09-05', '19:58:11', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (49, 'Totam rerum quo voluptas in.', 1, 3, 9, '2021-05-23', '05:14:40', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (50, 'Placeat ab nesciunt provident.', 2, 3, 43, '2021-08-05', '08:15:49', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (51, 'Id repudiandae possimus consectetur a.', 1, 61, 13, '2020-10-05', '12:55:40', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (52, 'Ad sed dolore molestiae qui.', 1, 58, 10, '2020-12-01', '12:04:54', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (53, 'Sed autem vel rem.', 3, 55, 39, '2021-03-11', '06:05:49', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (54, 'Aperiam in dolorum dolores.', 2, 58, 7, '2021-02-21', '16:50:55', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (55, 'Reiciendis ut ad distinctio temporibus accusamus rerum.', 3, 55, 43, '2021-03-14', '09:37:33', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (56, 'Quo cupiditate soluta cum exercitationem.', 2, 55, 7, '2021-09-02', '02:39:44', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (57, 'Mollitia reprehenderit in molestias quod fugiat.', 1, 54, 28, '2021-04-08', '10:21:26', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (58, 'Consequuntur molestiae nihil asperiores.', 1, 56, 4, '2021-09-01', '09:10:34', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (59, 'Eius aut non facere sint.', 2, 55, 52, '2021-02-05', '23:51:53', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (60, 'Est enim enim blanditiis impedit necessitatibus.', 3, 58, 34, '2020-11-10', '22:42:43', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (61, 'Ut tempora tenetur unde numquam maiores.', 2, 3, 43, '2021-09-12', '17:57:10', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (62, 'Asperiores excepturi porro rerum neque.', 1, 55, 42, '2021-05-19', '02:42:24', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (63, 'Rem nobis in et aut omnis.', 2, 61, 37, '2021-08-27', '10:33:49', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (64, 'Non tenetur possimus reprehenderit vel et.', 1, 57, 47, '2021-04-15', '13:59:21', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (65, 'Similique et quam et similique recusandae.', 3, 59, 28, '2021-07-01', '23:44:31', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (66, 'Aut facilis ea molestiae.', 1, 54, 33, '2020-12-22', '07:57:49', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (67, 'Saepe ducimus officia molestias voluptas.', 1, 56, 42, '2021-03-29', '11:21:24', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (68, 'Non quae repellendus voluptates cupiditate aut.', 1, 62, 50, '2020-10-12', '16:19:35', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (69, 'Doloribus unde itaque vel eum.', 1, 62, 18, '2021-04-12', '14:17:42', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (70, 'Quasi facilis expedita illum sunt dignissimos.', 2, 3, 31, '2021-08-19', '15:38:33', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (71, 'Sit numquam minima nam ut.', 2, 62, 5, '2021-03-10', '10:59:53', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (72, 'Quia necessitatibus deleniti assumenda quod quia.', 3, 57, 52, '2020-12-30', '20:56:53', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (73, 'Architecto in quo quod consectetur.', 3, 58, 24, '2021-06-12', '05:19:01', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (74, 'Temporibus officia ut quos pariatur minus.', 3, 57, 20, '2021-05-04', '23:22:41', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (75, 'Id temporibus necessitatibus consequatur quia est.', 3, 62, 51, '2021-01-12', '04:05:41', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (76, 'Aut et rem delectus corporis ut.', 1, 54, 50, '2021-08-10', '14:38:44', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (77, 'Quam natus rem excepturi repellat excepturi.', 3, 54, 43, '2020-09-24', '16:38:35', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (78, 'Fugit inventore in voluptas cum praesentium id.', 2, 58, 7, '2020-12-24', '07:32:07', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (79, 'Et quia molestiae sint voluptatem.', 2, 57, 16, '2021-03-08', '00:37:03', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (80, 'Fuga velit temporibus alias.', 3, 60, 35, '2020-11-12', '07:14:36', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (81, 'Est exercitationem quam veniam et.', 2, 3, 15, '2021-07-06', '07:24:37', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (82, 'Dolores accusamus placeat amet consequuntur.', 3, 62, 34, '2020-11-20', '17:29:17', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (83, 'Consectetur quisquam corporis assumenda eligendi aut.', 2, 56, 53, '2021-06-27', '22:57:50', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (84, 'Molestiae ad dignissimos iste.', 3, 56, 39, '2021-07-27', '16:22:42', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (85, 'Rem eaque laboriosam aut beatae.', 1, 3, 23, '2021-09-01', '19:43:55', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (86, 'Et voluptas qui et ut distinctio.', 2, 61, 18, '2020-12-07', '08:11:13', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (87, 'Velit labore deleniti sint.', 3, 55, 41, '2021-03-05', '06:24:17', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (88, 'Vel quibusdam et tenetur quis doloribus et.', 1, 59, 16, '2021-09-17', '21:36:42', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (89, 'Doloribus qui in nostrum quia.', 1, 62, 25, '2020-10-06', '02:39:34', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (90, 'Repellendus odio sequi nobis.', 2, 62, 49, '2021-01-14', '12:35:46', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (91, 'Quo distinctio libero omnis.', 1, 61, 44, '2021-06-05', '20:25:34', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (92, 'Et qui numquam nisi et ea odio.', 3, 60, 39, '2021-05-30', '11:06:58', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (93, 'Culpa est tempore totam sed vel voluptatum.', 2, 61, 44, '2020-12-14', '10:53:08', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (94, 'Voluptatem cupiditate et vero quisquam suscipit.', 1, 60, 35, '2021-05-29', '01:42:00', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (95, 'Nisi temporibus sed consequatur consequatur ea.', 1, 54, 49, '2020-11-25', '02:22:39', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (96, 'Eos eaque iure molestiae et.', 1, 58, 22, '2021-07-27', '17:12:38', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (97, 'Voluptatem sunt et earum.', 1, 3, 8, '2020-12-03', '13:06:35', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (98, 'Incidunt aut commodi non incidunt.', 2, 56, 45, '2020-10-29', '16:41:27', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (99, 'Ut voluptates excepturi molestiae eum.', 1, 60, 41, '2021-03-17', '10:07:46', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (100, 'Commodi nisi pariatur et.', 3, 3, 9, '2020-11-25', '23:06:45', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (101, 'Aspernatur et omnis iure repellendus nam.', 2, 59, 6, '2021-01-22', '04:44:14', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (102, 'Consectetur quia et explicabo fuga.', 1, 3, 21, '2021-02-28', '01:13:52', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (103, 'Sint atque ipsum cum fugit.', 3, 58, 6, '2021-03-25', '17:29:09', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (104, 'Incidunt quibusdam aut ipsam facere.', 3, 62, 29, '2021-07-27', '04:07:49', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (105, 'Enim et dolor aliquam saepe veritatis repudiandae.', 3, 54, 46, '2020-10-27', '01:23:19', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (106, 'Voluptas libero earum incidunt dolorem veniam labore.', 2, 60, 19, '2021-01-03', '00:23:02', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (107, 'Consequatur officia veritatis harum quibusdam et.', 2, 60, 15, '2021-01-04', '00:01:16', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (108, 'Ea magnam aut velit sunt repellendus.', 2, 62, 40, '2021-08-12', '13:09:07', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (109, 'Recusandae in porro perspiciatis sunt minus.', 2, 61, 48, '2020-11-13', '16:31:49', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (110, 'Eaque quia ad rerum quae est natus.', 1, 55, 14, '2021-06-17', '00:13:30', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (111, 'Aperiam eum quisquam consequuntur.', 2, 62, 32, '2020-11-16', '01:58:38', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (112, 'Officia praesentium omnis laudantium maiores sunt.', 2, 56, 5, '2021-03-18', '06:25:58', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (113, 'Rerum dolorem expedita fugit dicta.', 1, 56, 9, '2021-08-24', '18:49:42', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (114, 'Impedit soluta ut doloremque.', 3, 61, 30, '2021-08-10', '01:56:04', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (115, 'Ut est repellat provident.', 1, 59, 48, '2021-07-05', '11:56:03', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (116, 'Consequatur officiis necessitatibus et.', 1, 55, 24, '2021-03-03', '22:22:39', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (117, 'Minima molestiae quia natus doloribus.', 3, 62, 15, '2021-04-08', '09:08:57', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (118, 'Molestiae et debitis dolor.', 2, 58, 43, '2021-05-31', '00:41:13', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (119, 'Quasi perferendis repudiandae perspiciatis dolore quia molestiae.', 2, 60, 45, '2021-07-12', '16:34:27', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (120, 'Soluta id sapiente tenetur harum numquam ut.', 1, 55, 40, '2021-08-11', '06:15:18', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (121, 'Voluptate consequuntur amet vitae eum ut culpa.', 2, 58, 46, '2021-05-01', '12:53:43', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (122, 'Optio asperiores qui tempore doloremque doloribus reprehenderit.', 2, 60, 52, '2021-04-01', '00:46:56', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (123, 'Dignissimos ut excepturi et atque adipisci iusto.', 2, 56, 40, '2021-01-13', '18:39:40', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (124, 'Quo minima omnis vero a totam magni.', 1, 54, 32, '2021-09-02', '14:32:20', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (125, 'Aut corporis voluptas sunt.', 3, 59, 49, '2020-11-05', '02:54:21', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (126, 'Deserunt autem quas voluptatem magni.', 1, 62, 27, '2021-09-14', '07:02:27', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (127, 'Totam est voluptate rerum.', 1, 58, 21, '2020-11-21', '12:16:44', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (128, 'Magni unde velit eum perferendis.', 1, 57, 5, '2021-06-28', '01:52:38', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (129, 'Beatae id occaecati doloremque.', 2, 55, 2, '2021-05-29', '00:06:04', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (130, 'Quasi et et harum officia qui.', 1, 54, 49, '2021-03-16', '14:29:04', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (131, 'Ex molestiae laborum fugiat distinctio.', 3, 3, 21, '2021-07-19', '17:39:53', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (132, 'Et possimus quisquam quia assumenda est.', 2, 56, 32, '2021-05-19', '03:12:37', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (133, 'Ab incidunt quis aspernatur.', 2, 56, 40, '2021-07-05', '18:15:31', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (134, 'Alias exercitationem iste quis debitis dolor.', 2, 62, 45, '2021-08-13', '04:18:43', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (135, 'Vero amet natus molestiae officiis quae.', 3, 55, 14, '2020-11-16', '04:24:44', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (136, 'Aperiam quidem eligendi eum earum nihil excepturi.', 1, 62, 48, '2021-05-15', '13:36:56', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (137, 'Eligendi ipsam consequatur iste consequuntur.', 1, 59, 44, '2021-05-26', '14:42:28', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (138, 'Et sed molestias rerum aliquam ipsum tempora.', 3, 59, 8, '2021-01-23', '19:01:12', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (139, 'Et culpa a ea voluptas.', 3, 3, 44, '2021-06-20', '18:45:47', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (140, 'Nam temporibus aut laborum a distinctio.', 1, 54, 47, '2021-02-05', '05:33:49', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (141, 'Et laborum et id amet repudiandae recusandae.', 2, 62, 20, '2021-06-17', '15:02:35', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (142, 'Eius cupiditate neque quisquam.', 3, 58, 9, '2021-08-09', '09:09:41', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (143, 'Ut et eligendi in accusantium dolorem.', 2, 54, 14, '2020-09-24', '12:46:36', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (144, 'Quasi qui quasi nam nisi.', 1, 61, 31, '2021-01-03', '15:22:14', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (145, 'Expedita sed mollitia impedit est enim.', 3, 58, 33, '2020-10-26', '12:17:48', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (146, 'Similique corporis aut ab.', 1, 59, 51, '2021-08-01', '06:35:09', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (147, 'Et accusamus amet enim sed occaecati.', 2, 57, 39, '2021-05-08', '02:07:03', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (148, 'Sit quis sint vitae qui cupiditate adipisci.', 3, 57, 49, '2020-09-23', '06:35:42', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (149, 'Nihil esse doloremque debitis eos autem.', 1, 61, 16, '2021-05-23', '21:49:26', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (150, 'Magnam repellendus vitae enim voluptatem.', 1, 3, 37, '2020-11-15', '23:01:23', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (151, 'Magni nulla voluptatibus ut.', 2, 55, 50, '2020-10-15', '21:00:49', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (152, 'Eaque non impedit aperiam voluptas exercitationem nobis.', 1, 3, 2, '2021-02-16', '04:49:09', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (153, 'Id quo suscipit sed sit nesciunt.', 1, 62, 33, '2021-01-18', '19:18:13', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (154, 'Sint ad enim qui voluptas.', 1, 56, 30, '2021-07-04', '06:26:37', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (155, 'Sapiente assumenda voluptas magnam quia aspernatur.', 1, 61, 15, '2021-04-07', '21:44:39', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (156, 'Voluptatibus sit est cupiditate.', 1, 54, 27, '2021-08-06', '03:23:05', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (157, 'Harum harum asperiores quisquam similique.', 2, 61, 29, '2021-06-06', '22:20:25', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (158, 'Qui asperiores inventore consequatur et necessitatibus.', 1, 59, 9, '2021-01-07', '09:51:50', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (159, 'Et non suscipit consequatur.', 3, 54, 9, '2021-02-19', '07:06:53', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (160, 'Aut commodi officiis esse aperiam facilis quibusdam.', 2, 57, 16, '2020-11-05', '00:03:28', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (161, 'Tempora mollitia qui quis voluptas minima officia.', 1, 57, 46, '2020-12-17', '02:18:33', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (162, 'Distinctio iste id voluptatem in debitis.', 2, 60, 51, '2020-10-19', '05:12:05', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (163, 'Recusandae aperiam ut asperiores.', 1, 55, 5, '2021-05-26', '00:53:55', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (164, 'Et pariatur eveniet sed autem architecto.', 2, 62, 5, '2021-01-11', '22:24:26', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (165, 'Repellat blanditiis ad dolores nisi.', 3, 58, 25, '2020-12-24', '19:09:33', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (166, 'Est aut sit non aliquam.', 1, 57, 48, '2021-07-25', '05:56:49', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (167, 'Sunt harum commodi dolor quod error.', 2, 62, 31, '2021-09-07', '19:17:16', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (168, 'Aut eum atque eum asperiores deserunt.', 2, 57, 53, '2021-03-04', '20:18:35', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (169, 'Itaque et illum similique sunt ut.', 2, 57, 8, '2020-11-25', '04:21:55', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (170, 'Maiores nihil sequi ducimus adipisci nihil.', 1, 57, 9, '2021-08-03', '12:15:38', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (171, 'Voluptatem sapiente quisquam doloribus ut veniam voluptatem.', 2, 61, 15, '2021-09-09', '12:02:59', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (172, 'Ipsam itaque et quaerat quod laboriosam et.', 1, 58, 52, '2021-05-14', '10:45:01', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (173, 'Illo dicta qui sed in vero.', 2, 58, 23, '2021-07-16', '00:30:25', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (174, 'Sit est aut iste.', 1, 62, 36, '2021-09-01', '12:02:16', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (175, 'Explicabo omnis qui labore dolorem ipsam facere.', 2, 59, 9, '2020-11-25', '03:28:56', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (176, 'Esse qui magni officiis ut.', 1, 61, 34, '2021-04-29', '16:09:31', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (177, 'Labore ex dolor et dolores non quaerat voluptatem.', 2, 59, 9, '2021-08-21', '02:55:14', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (178, 'Soluta aliquid dignissimos impedit.', 1, 58, 33, '2020-10-12', '01:54:43', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (179, 'Quia quos magnam qui qui dignissimos tempore.', 2, 54, 37, '2021-09-03', '09:42:49', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (180, 'Voluptate itaque porro voluptate provident amet.', 2, 56, 37, '2021-04-02', '21:28:46', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (181, 'Saepe fugit sed praesentium atque aut.', 3, 56, 31, '2020-11-18', '22:58:59', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (182, 'Magni fuga et sint saepe.', 3, 58, 53, '2021-01-30', '02:58:27', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (183, 'Fugiat asperiores natus reprehenderit provident sapiente fugiat.', 2, 59, 16, '2021-09-09', '12:11:47', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (184, 'Est rem veritatis sit occaecati.', 2, 57, 41, '2021-01-13', '19:38:36', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (185, 'Voluptatum distinctio qui quia ratione aut.', 1, 54, 27, '2021-09-14', '19:24:55', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (186, 'Sed sit aut omnis deserunt tenetur nisi et.', 2, 62, 43, '2021-01-03', '14:24:58', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (187, 'Distinctio asperiores occaecati ducimus quo fuga minima.', 1, 57, 41, '2021-03-31', '13:59:38', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (188, 'Sunt quas placeat qui.', 1, 62, 45, '2020-09-28', '04:05:00', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (189, 'Ea nihil tenetur nulla libero.', 2, 61, 49, '2021-08-23', '00:26:45', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (190, 'Esse sed assumenda sapiente unde velit dolorem.', 1, 60, 5, '2021-04-19', '14:19:08', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (191, 'Facere molestiae laborum sunt maiores velit est.', 2, 57, 53, '2021-03-12', '10:34:27', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (192, 'Mollitia qui reprehenderit illo sint et eligendi.', 3, 60, 17, '2021-03-21', '09:30:30', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (193, 'Earum et quidem reiciendis expedita dolores.', 2, 56, 21, '2021-02-13', '05:30:15', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (194, 'Quisquam fugiat quod laborum similique.', 3, 56, 50, '2021-05-24', '15:08:22', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (195, 'Voluptatibus incidunt doloremque vitae.', 3, 55, 20, '2021-04-26', '06:14:41', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (196, 'Rerum aperiam quis quos numquam perspiciatis iure.', 3, 62, 23, '2021-06-25', '17:48:54', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (197, 'Dicta autem laborum non sapiente.', 2, 59, 22, '2020-12-30', '07:34:28', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (198, 'Ut impedit sed sunt.', 2, 57, 53, '2021-05-06', '17:24:01', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (199, 'Eligendi voluptas pariatur rerum unde.', 3, 55, 40, '2021-02-16', '18:59:19', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (200, 'Eos beatae ut quos hic.', 3, 58, 39, '2020-10-12', '18:07:02', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (201, 'Mollitia modi unde quaerat labore.', 2, 3, 15, '2021-02-13', '10:55:36', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (202, 'Ut exercitationem aut animi sequi.', 2, 54, 46, '2020-11-01', '20:29:09', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (203, 'Veritatis dolores doloremque recusandae ratione quisquam.', 2, 61, 46, '2021-05-04', '04:29:49', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (204, 'Ut aliquam maxime esse est.', 2, 3, 13, '2021-05-19', '18:06:54', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (205, 'Ipsa modi iure veniam incidunt.', 1, 59, 32, '2021-08-15', '19:59:13', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (206, 'Porro vero eius qui.', 1, 60, 50, '2021-01-12', '14:59:56', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (207, 'Deserunt nemo eum ullam libero.', 1, 62, 33, '2021-05-10', '07:16:54', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (208, 'Pariatur ipsum molestiae dolorem.', 1, 61, 38, '2020-10-14', '10:57:42', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (209, 'Qui quia aliquid ea.', 3, 62, 46, '2020-11-08', '11:03:36', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (210, 'Itaque aperiam et voluptatem vel minus.', 1, 3, 4, '2020-10-13', '22:45:19', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (211, 'Beatae qui deserunt ea incidunt iusto ut praesentium.', 3, 3, 47, '2021-06-25', '13:51:12', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (212, 'Quas optio harum harum et doloremque.', 3, 58, 6, '2021-08-13', '17:44:25', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (213, 'Dignissimos eligendi vel excepturi animi.', 3, 3, 48, '2020-10-13', '03:38:21', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (214, 'Aut suscipit eos consequatur atque est.', 2, 56, 40, '2020-12-27', '06:58:36', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (215, 'Fugit qui et corrupti aperiam.', 2, 54, 15, '2021-05-24', '17:55:49', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (216, 'Suscipit voluptatem qui nostrum.', 3, 60, 20, '2021-01-30', '03:23:45', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (217, 'Optio placeat dolores est temporibus vel.', 2, 55, 2, '2021-07-16', '15:17:06', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (218, 'Tenetur provident vel reiciendis facilis impedit.', 1, 58, 34, '2021-07-31', '04:22:15', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (219, 'Molestiae odit eius qui eveniet.', 1, 60, 26, '2021-02-09', '12:55:34', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (220, 'Esse ut beatae ea sit.', 2, 55, 10, '2021-04-26', '00:14:38', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (221, 'Et occaecati fuga saepe autem aut.', 2, 54, 47, '2021-05-04', '04:34:04', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (222, 'Dignissimos nihil consectetur assumenda quis.', 2, 60, 17, '2021-09-16', '09:20:40', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (223, 'Omnis ipsam veniam ea.', 3, 59, 6, '2020-12-27', '13:52:50', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (224, 'Atque libero consequatur incidunt id provident.', 1, 61, 28, '2020-10-04', '22:14:11', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (225, 'Quis ratione accusamus aperiam iste.', 3, 56, 53, '2021-08-06', '19:54:23', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (226, 'Non odit ea dolores vel ut iusto.', 2, 55, 50, '2021-03-11', '14:18:18', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (227, 'Dolorem unde ab dolore consequuntur.', 3, 57, 10, '2021-07-17', '22:46:42', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (228, 'Asperiores at consequatur cum assumenda vitae.', 1, 55, 16, '2021-01-25', '15:54:29', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (229, 'Velit non porro tenetur.', 2, 62, 40, '2021-08-04', '18:28:08', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (230, 'Voluptas quis eum culpa.', 1, 61, 41, '2020-10-23', '23:40:05', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (231, 'Et nobis distinctio rerum vero sed omnis.', 3, 57, 36, '2021-05-02', '22:01:47', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (232, 'Aut deserunt repellendus est neque molestiae.', 2, 60, 25, '2021-09-05', '11:58:19', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (233, 'Reiciendis voluptatem consequatur deleniti eos.', 1, 62, 48, '2020-11-19', '17:18:48', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (234, 'Neque voluptas adipisci ut et.', 3, 3, 33, '2021-01-16', '08:41:26', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (235, 'Adipisci nisi nesciunt consequatur sit adipisci voluptatem.', 2, 60, 9, '2021-02-28', '06:47:57', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (236, 'Sed laboriosam neque aut quia.', 1, 55, 35, '2020-10-29', '04:14:05', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (237, 'Esse ducimus et facilis maxime quisquam aut.', 1, 59, 35, '2020-12-18', '20:27:16', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (238, 'Amet libero ut quaerat adipisci sunt id dicta.', 3, 58, 32, '2021-07-14', '16:07:03', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (239, 'Blanditiis qui eveniet repellendus quia.', 1, 59, 37, '2021-04-08', '12:55:42', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (240, 'Amet minima consequatur animi et ea quisquam odio.', 2, 57, 28, '2021-06-19', '10:13:52', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (241, 'Veniam adipisci corporis quis quia.', 3, 61, 41, '2021-04-17', '02:35:57', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (242, 'Et ipsam rem id qui iusto omnis.', 3, 57, 22, '2021-07-21', '17:07:00', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (243, 'Laudantium qui dignissimos qui et aliquid.', 1, 3, 34, '2021-04-10', '15:17:12', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (244, 'Voluptate harum molestiae odit laborum voluptatem corporis.', 3, 61, 12, '2020-10-29', '18:00:48', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (245, 'Libero nulla vel quas omnis.', 3, 57, 10, '2021-07-09', '09:35:56', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (246, 'Ipsam dolor veritatis explicabo natus.', 2, 3, 9, '2021-08-27', '20:05:08', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (247, 'Odit culpa voluptas voluptatem consequatur aut repellendus.', 1, 60, 37, '2021-03-24', '23:24:22', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (248, 'Distinctio cum a ut.', 1, 56, 52, '2020-10-24', '21:35:33', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (249, 'Quia est rem est alias architecto.', 3, 61, 31, '2020-10-04', '10:24:10', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (250, 'Consequatur qui voluptatem est autem.', 1, 61, 19, '2021-03-18', '19:54:47', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (251, 'Doloremque asperiores quod earum aut voluptates.', 3, 59, 51, '2021-03-12', '14:50:41', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (252, 'Et minus voluptas ab voluptas qui et.', 2, 3, 34, '2021-03-07', '22:37:16', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (253, 'Molestias quod aliquid tenetur.', 3, 62, 4, '2020-10-25', '22:08:06', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (254, 'Voluptatibus libero at quis aut delectus.', 2, 61, 40, '2020-11-24', '06:41:38', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (255, 'Eos aut qui ut minima.', 2, 3, 13, '2020-10-27', '12:22:51', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (256, 'Ducimus ab ullam enim voluptatem.', 1, 3, 2, '2021-07-18', '10:07:05', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (257, 'Laudantium aut tenetur quibusdam.', 3, 56, 43, '2021-03-09', '15:18:43', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (258, 'Ut commodi debitis autem sit eum.', 1, 54, 17, '2021-01-24', '20:01:36', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (259, 'Velit atque vitae voluptates.', 2, 61, 28, '2021-03-19', '21:21:18', 'Operación', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Cancelada');
INSERT INTO `appointments` VALUES (260, 'Ut et voluptatibus asperiores expedita voluptate.', 2, 54, 13, '2021-07-26', '21:41:56', 'Consulta', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (261, 'Nostrum unde fuga doloribus nemo aliquam nihil.', 3, 54, 19, '2021-04-23', '10:45:33', 'Examen', '2021-09-18 12:53:01', '2021-09-18 12:53:01', 'Atendida');
INSERT INTO `appointments` VALUES (262, 'Illum sit esse libero et facere et.', 3, 55, 40, '2021-04-23', '04:53:57', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (263, 'Provident nihil velit dolor.', 2, 56, 34, '2021-08-08', '06:01:18', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (264, 'A magnam aliquam quod.', 2, 55, 35, '2021-01-06', '11:02:45', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (265, 'Repudiandae sit autem distinctio.', 1, 54, 51, '2020-11-14', '19:35:49', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (266, 'Et consequatur et repellat et.', 3, 59, 2, '2021-02-09', '03:08:18', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (267, 'Repellendus mollitia assumenda consequatur deserunt doloremque.', 3, 58, 39, '2021-04-13', '14:44:39', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (268, 'Non non quae ut unde dolores.', 2, 61, 30, '2021-07-15', '04:26:03', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (269, 'Est error reiciendis voluptas ut aliquam.', 1, 3, 7, '2021-06-04', '03:04:23', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (270, 'Est alias vel magnam.', 2, 55, 14, '2021-04-09', '23:11:33', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (271, 'Ut soluta aperiam laudantium.', 3, 57, 35, '2020-10-12', '14:54:36', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (272, 'Voluptas sit quaerat quam.', 2, 57, 27, '2021-03-21', '22:18:07', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (273, 'Aliquid optio voluptatem quia quia.', 2, 55, 6, '2021-07-18', '09:22:46', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (274, 'Necessitatibus ducimus itaque impedit temporibus fuga ullam in.', 2, 59, 36, '2021-05-09', '19:05:04', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (275, 'Nobis odit accusantium fuga et molestias.', 2, 58, 15, '2021-01-24', '20:38:19', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (276, 'Molestiae adipisci eius consequatur excepturi explicabo.', 2, 3, 29, '2020-10-26', '04:32:54', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (277, 'Sunt velit deleniti iste iusto.', 3, 56, 20, '2020-12-02', '08:33:05', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (278, 'Et qui quia autem libero accusamus.', 2, 57, 53, '2021-07-30', '19:32:23', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (279, 'Ut quasi illum quos.', 3, 62, 12, '2020-12-22', '19:42:54', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (280, 'Vel fugit expedita sint quasi est officiis.', 1, 3, 45, '2020-12-19', '13:56:17', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (281, 'Pariatur non ea nulla debitis.', 2, 56, 52, '2020-11-27', '06:39:39', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (282, 'Doloribus dolores adipisci perferendis ad harum.', 3, 55, 10, '2020-11-29', '13:11:11', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (283, 'Cum exercitationem sed tenetur nostrum officia.', 3, 62, 17, '2021-04-19', '04:03:17', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (284, 'Maiores at magnam nihil asperiores architecto.', 3, 57, 49, '2020-10-28', '05:38:40', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (285, 'Libero est praesentium cumque dolorem laudantium atque.', 2, 55, 49, '2020-12-12', '01:47:49', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (286, 'Ut sequi ipsam explicabo odit.', 2, 57, 31, '2021-07-13', '04:26:55', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (287, 'Dolorem consequatur molestias ullam saepe enim.', 3, 58, 18, '2021-06-01', '10:02:50', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (288, 'Qui excepturi non unde fugiat minima.', 2, 56, 22, '2021-05-18', '21:04:23', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (289, 'Porro vero eius et quas.', 3, 54, 32, '2021-02-11', '22:32:01', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');
INSERT INTO `appointments` VALUES (290, 'Magni vel et ut ullam porro.', 2, 58, 24, '2021-04-23', '19:22:22', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (291, 'Omnis ut aut voluptates et.', 3, 56, 27, '2021-01-19', '03:56:40', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (292, 'Voluptas commodi sit culpa laudantium fuga.', 1, 60, 20, '2021-01-30', '07:11:46', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (293, 'Eos et voluptatem officia suscipit deserunt.', 3, 60, 39, '2021-03-23', '02:21:12', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (294, 'Sed illo quam accusantium aperiam.', 1, 54, 9, '2020-10-19', '06:46:14', 'Consulta', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (295, 'Aspernatur enim ut harum velit sequi et dolores.', 3, 55, 34, '2021-05-28', '12:35:18', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (296, 'Asperiores autem corrupti voluptatem eum totam.', 1, 57, 2, '2021-01-14', '07:45:45', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (297, 'Rerum pariatur eum ex et sed.', 1, 57, 34, '2021-08-03', '16:46:25', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (298, 'Voluptatum est corporis sunt.', 3, 54, 11, '2021-07-29', '21:04:50', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (299, 'Praesentium sequi minus assumenda molestiae.', 2, 3, 13, '2021-08-05', '18:32:47', 'Operación', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Cancelada');
INSERT INTO `appointments` VALUES (300, 'Rerum sit tenetur numquam.', 2, 54, 37, '2021-08-30', '19:56:53', 'Examen', '2021-09-18 12:53:02', '2021-09-18 12:53:02', 'Atendida');

-- ----------------------------
-- Table structure for cancelled_appointments
-- ----------------------------
DROP TABLE IF EXISTS `cancelled_appointments`;
CREATE TABLE `cancelled_appointments`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `appointment_id` int(10) UNSIGNED NOT NULL,
  `justification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `cancelled_by_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cancelled_appointments_appointment_id_foreign`(`appointment_id`) USING BTREE,
  INDEX `cancelled_appointments_cancelled_by_foreign`(`cancelled_by_id`) USING BTREE,
  CONSTRAINT `cancelled_appointments_appointment_id_foreign` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cancelled_appointments_cancelled_by_foreign` FOREIGN KEY (`cancelled_by_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cancelled_appointments
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (11, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (12, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (13, '2018_10_13_144808_create_specialties_table', 1);
INSERT INTO `migrations` VALUES (14, '2018_10_17_134011_create_work_days_table', 1);
INSERT INTO `migrations` VALUES (15, '2018_10_21_142602_create_appointments_table', 1);
INSERT INTO `migrations` VALUES (16, '2018_10_21_160731_create_specialty_user_table', 1);
INSERT INTO `migrations` VALUES (17, '2018_11_02_231542_add_status_to_appointments', 1);
INSERT INTO `migrations` VALUES (18, '2018_11_06_044738_create_cancelled_appointments_table', 1);
INSERT INTO `migrations` VALUES (19, '2018_11_06_152547_rename_cancelled_by_in_cancelled_appointments_table', 1);
INSERT INTO `migrations` VALUES (20, '2018_11_28_220848_add_device_token_to_users', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for specialties
-- ----------------------------
DROP TABLE IF EXISTS `specialties`;
CREATE TABLE `specialties`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of specialties
-- ----------------------------
INSERT INTO `specialties` VALUES (1, 'Oftalmología', NULL, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialties` VALUES (2, 'Pediatría', NULL, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialties` VALUES (3, 'Neurología', NULL, '2021-09-18 12:53:00', '2021-09-18 12:53:00');

-- ----------------------------
-- Table structure for specialty_user
-- ----------------------------
DROP TABLE IF EXISTS `specialty_user`;
CREATE TABLE `specialty_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `specialty_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `specialty_user_user_id_foreign`(`user_id`) USING BTREE,
  INDEX `specialty_user_specialty_id_foreign`(`specialty_id`) USING BTREE,
  CONSTRAINT `specialty_user_specialty_id_foreign` FOREIGN KEY (`specialty_id`) REFERENCES `specialties` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `specialty_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of specialty_user
-- ----------------------------
INSERT INTO `specialty_user` VALUES (1, 54, 1, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialty_user` VALUES (2, 55, 1, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialty_user` VALUES (3, 56, 1, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialty_user` VALUES (4, 57, 2, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialty_user` VALUES (5, 58, 2, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialty_user` VALUES (6, 59, 2, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialty_user` VALUES (7, 60, 3, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialty_user` VALUES (8, 61, 3, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialty_user` VALUES (9, 62, 3, '2021-09-18 12:53:00', '2021-09-18 12:53:00');
INSERT INTO `specialty_user` VALUES (10, 3, 3, '2021-09-18 12:53:00', '2021-09-18 12:53:00');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `device_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Juan Ramos', 'administrador@gmail.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', NULL, NULL, NULL, 'admin', 'ur1gQzkBGXg8X5J5V3e1bP2hVrrT9JazzNgYy0WJBN9j7cK3pDka74cXUW72', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (2, 'Paciente Test', 'paciente@gmail.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', NULL, 'La nueva jerusalen', '930648346', 'patient', 'zBVRVPUrHhlBKuIMf7VzA4WX02kxL1doqn0WU4FdtDhHu9idZ49AHYCkWKmY', '2021-09-18 12:53:00', '2021-09-18 13:56:41', NULL);
INSERT INTO `users` VALUES (3, 'Médico Test', 'doctor@gmail.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', NULL, NULL, NULL, 'doctor', '71dFG64z66L6dCzuDTmOD4XLz3C4QKEvNxWRe8L9z6XdI86IGnJbw1Z7SyL7', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (4, 'Serenity Pollich', 'bertrand.hessel@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '36464782', '86545 Makayla Rest\nBriellehaven, ND 10036', '+8712550829123', 'patient', 'AMysrgWAzD', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (5, 'Karina Dibbert', 'zmedhurst@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '74836890', '5868 Adonis Fall Suite 479\nNorth Eulah, NY 60210-4240', '+8739883849819', 'patient', 'C1NiJ14vfD', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (6, 'Miss Sincere Lockman', 'imorissette@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '59048085', '4053 Treutel Parks Apt. 527\nPowlowskihaven, LA 02796', '+5809678997528', 'patient', '5RTRRWMAi7', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (7, 'Jaunita Wisozk', 'emmett42@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '39465651', '726 Lura Drive Suite 718\nEveborough, NY 31033', '+4059430209239', 'patient', 'QlUYJnLlkI', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (8, 'Dr. Markus Spencer', 'cayla.carroll@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '27026870', '35898 Pouros Station Suite 422\nBudport, AL 86530', '+7013915476465', 'patient', 'WAthpZis1d', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (9, 'Darius Fisher', 'maybell.wiza@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '48004681', '8685 Grady Groves\nNew Summer, AL 29553', '+3643299829610', 'patient', '5pC6Vq1zBi', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (10, 'Constance Beier', 'nyasia51@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '11558039', '551 Larson Mission\nLake Zariaview, MD 27255-0894', '+2517523166634', 'patient', 'NoscDukll3', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (11, 'John Hamill DVM', 'ken37@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '23762137', '5077 Wilderman Road\nFritschview, AR 32023-1398', '+9249371289289', 'patient', 'MghWlEnnCL', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (12, 'Eldora O\'Kon', 'rickie.kozey@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '38448508', '1021 Rath Summit\nSouth Margueriteton, UT 31272-6916', '+6168161377526', 'patient', 'zKye18zrIK', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (13, 'Dr. Rodolfo Schmidt I', 'patrick82@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '73299405', '420 Britney Springs\nLinnieshire, AZ 81604', '+4147169487624', 'patient', 'Bcj2b9q4M7', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (14, 'Chelsey Kessler', 'zackery.padberg@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '67467384', '96531 Stracke Port Apt. 398\nCatalinamouth, AZ 74401-0898', '+8958859215242', 'patient', '1S0NtBq19h', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (15, 'Adrianna Rosenbaum V', 'penelope.larkin@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '59230039', '774 Katrina Brook Apt. 162\nLubowitzton, RI 03002-8292', '+5673362862574', 'patient', 'kjIfeXsFwY', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (16, 'Ruthie Pfannerstill', 'ruecker.bertha@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '50554542', '592 DuBuque Island Suite 769\nSabrynaport, CA 81900', '+8418217001595', 'patient', 'LccRNJ3tlU', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (17, 'Lucienne Carter', 'mbrekke@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '79406960', '84845 Smitham Creek\nOdaside, NH 35421-1751', '+2737281371732', 'patient', '5bT5CafjZy', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (18, 'Aniya Bailey', 'elyssa.flatley@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '46969958', '5292 Bernhard Village Suite 315\nPort Julian, IA 33892', '+5567799540094', 'patient', '5JMGkG4K3b', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (19, 'Dr. Bridie Grady Jr.', 'jazlyn91@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '39685696', '47966 Wuckert Heights Apt. 928\nSanfordshire, MN 86787-0419', '+3209495974071', 'patient', '9U7BKQuImu', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (20, 'Dominic Walter', 'kellie.bechtelar@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '60807144', '76120 Oliver Lane\nHoppeborough, AZ 32561-3986', '+2768720608287', 'patient', 'cby7zPCNq6', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (21, 'Everett Nicolas', 'xkunze@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '22549033', '9996 Hessel Trail Suite 256\nPort Jessie, AK 82450', '+6289745679363', 'patient', 'gkAtJOHVKx', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (22, 'Dana Hill', 'deven23@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '72365933', '70544 Moen Path\nChanelleshire, KY 98853', '+2007811703655', 'patient', 'NgG7IrpxAn', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (23, 'Mr. Stone Parisian', 'sarai23@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '94039765', '67829 Kuhic Radial\nWest Carmel, MT 37381', '+7787260451482', 'patient', 'Y93EMf5vI7', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (24, 'Ms. Brionna Paucek', 'brakus.maynard@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '60700226', '67354 Frami Extension Apt. 702\nKihnburgh, TN 94053', '+2517616568242', 'patient', '0V4sbhuK62', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (25, 'Ms. Joelle Sipes', 'mhamill@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '91161785', '2909 Torphy Cape Apt. 696\nRosenbaumberg, PA 60719-4003', '+8503657641666', 'patient', '4bdhVGOFMy', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (26, 'Miss Muriel Dickinson', 'barmstrong@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '37637161', '8802 Federico Junctions\nMyronchester, WA 41894', '+8481193167435', 'patient', 'brevgDOTzL', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (27, 'Burnice Kuhic', 'ojast@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '82278001', '9849 Medhurst Forge Suite 886\nKleinmouth, GA 21294', '+3569737502778', 'patient', 'qUauiR0fs1', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (28, 'Ms. Rosemary Bernier', 'monahan.josue@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '50129862', '6744 Reinger Keys\nNorth Hazel, AL 08288-1271', '+3333495605648', 'patient', 'eIyIFBWpsN', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (29, 'Maryam Gusikowski', 'cullen04@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '24261609', '946 Huel Inlet\nEast Sebastianside, WV 95776-6868', '+8952804160663', 'patient', 'KRrfndQyA0', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (30, 'Kamren Gutmann', 'bfeeney@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '36704165', '3857 Schaefer Curve\nEast Jonatan, UT 33257-0908', '+2541732118857', 'patient', '6fMazi5QKL', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (31, 'Mr. Ricky Reichel I', 'stamm.laisha@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '15885689', '461 Gus Spring Suite 878\nDavisberg, KS 60746', '+5037104563660', 'patient', 'bo8ImzPc1D', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (32, 'Prof. Harmony Kunze III', 'laurence.langosh@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '94686571', '4227 Jovany Summit Apt. 146\nPort Toyfurt, FL 43104', '+1311449533723', 'patient', 'znTzK5ee8F', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (33, 'Junior Kris', 'rhodkiewicz@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '41446118', '8929 Mikel Extension\nNew Roberto, WI 98798', '+4168893083567', 'patient', '765vsgQdce', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (34, 'Luna Harvey I', 'alana68@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '76155364', '307 Nienow Brook\nOrtizborough, AL 87069-3595', '+5646365362624', 'patient', '0Mq0L9nTjq', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (35, 'Dorian Koch', 'patsy32@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '27332959', '9400 Yundt Drive\nSouth Roman, MS 20728-9507', '+6295798401036', 'patient', 'upleHsiS81', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (36, 'Evangeline Labadie', 'ward.trever@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '97444159', '32096 Desmond Turnpike Suite 876\nLake Meganeshire, WA 40480-6959', '+7053751087727', 'patient', 'cRG7CCdo1H', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (37, 'Fern Conroy', 'liza69@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '71420332', '61543 Kunze Views\nRaynorfort, CT 79465-8944', '+9784406113317', 'patient', 'ZjGqQIQmzQ', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (38, 'Mr. Cecil Homenick', 'renner.amparo@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '32704218', '822 Amely Brooks\nLake Santiago, DE 91000-9772', '+7928728588763', 'patient', '409bJfrO6Z', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (39, 'Marcelo Mann', 'vern12@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '86669868', '70322 Blanda Plain Apt. 297\nNew Lonie, DC 05750', '+9604988212935', 'patient', 'MIO8zB5zeH', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (40, 'Deangelo Bartoletti DVM', 'dmurazik@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '53607331', '62586 Jast Cliffs\nEast Rita, VA 51412-8899', '+2541600290512', 'patient', 'zJpnxhdtLE', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (41, 'Itzel Frami IV', 'trinity88@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '71160684', '57418 Rosalinda Estate Suite 947\nNorth Lanechester, LA 57265', '+2771399082579', 'patient', '9RigunU7S1', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (42, 'Jazmyne Dickinson', 'bmertz@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '63536426', '567 Kurtis Corners Suite 255\nWest Zion, LA 22597-5204', '+5389634942700', 'patient', 'Xex5EAjXZM', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (43, 'Erick Kertzmann', 'emmerich.florian@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '17071079', '15816 Larson Pines\nLake Justenport, IN 93523', '+2342186786378', 'patient', 'cROyGI9gOH', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (44, 'Seth Corkery DDS', 'stroman.morton@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '59998146', '2017 Carter Passage\nSanfordview, AL 43066-7334', '+8893220936086', 'patient', 'qGBHMGOZc9', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (45, 'Miss Maiya Schmidt', 'bbernhard@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '99632202', '2838 Blick Mountains\nMarjorieton, WY 82829-7843', '+4848555135471', 'patient', 'I3Pyj5JhRh', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (46, 'Ms. Felicity Powlowski', 'alverta50@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '92132992', '53654 Reynolds Manor\nEliville, KS 92871-5572', '+5108510915764', 'patient', 'SIxMymlSFD', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (47, 'Maximilian Steuber', 'kcronin@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '70248660', '5100 Sawayn Rest\nOletafort, MA 52518-8983', '+5651526905896', 'patient', 'YV8K3ZtfGT', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (48, 'Chadd O\'Connell', 'mraz.shaniya@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '44724168', '52401 Herminia Centers\nWest Desiree, RI 23225', '+1575889259611', 'patient', 'A9O6eNf07I', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (49, 'Jaquan Ferry', 'ewitting@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '63494149', '53932 Hilpert Extensions\nSylvialand, RI 54084-1734', '+7024305822627', 'patient', 'dTJ6r28cKE', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (50, 'Oral O\'Conner', 'gerda.jacobs@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '50157368', '87637 Metz Groves Apt. 618\nEast Jazlynport, OH 96244-8728', '+6271673897375', 'patient', '7SbQkiKFBL', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (51, 'Miss Libby Waters MD', 'meda.mclaughlin@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '57848127', '23448 Turcotte Mission Suite 130\nPort Eryn, SD 41433', '+5251322030433', 'patient', 'yWes3MnlWQ', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (52, 'Dr. Earnest Leannon Sr.', 'benton59@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '83861352', '64509 Jimmy Overpass Apt. 369\nMonahanshire, KY 47234', '+2936566233789', 'patient', 'O2B5FYb4xW', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (53, 'Wilson Hill', 'cruickshank.hailie@example.net', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '21691391', '5950 Zula Port Apt. 810\nSouth Eleanore, UT 39041-2458', '+2987225353340', 'patient', 'RBLy7T1f49', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (54, 'Amanda Lockman', 'srice@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '50773068', '13327 Oscar Terrace\nNew Geraldineborough, AZ 37641-5093', '+7127421186758', 'doctor', 'S1ZNKN10tM', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (55, 'Tate Fahey', 'declan.botsford@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '84875870', '38132 Heaney Mount Apt. 909\nLake Aliyaland, IA 34494-1127', '+9381448729959', 'doctor', 'nkiQSm1Dxt', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (56, 'Evangeline Mraz', 'alexanne.ryan@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '50796137', '5008 Domenica Falls Suite 197\nLake Xzavier, IN 31326-1563', '+8077852708309', 'doctor', 'LvsLPE22ow', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (57, 'Keyon Conn', 'muriel.nienow@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '59029938', '179 Zetta Landing\nNew Godfrey, VA 86096-2362', '+7102994410106', 'doctor', 'Q7iAGTCXgH', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (58, 'Brycen Skiles', 'annabelle97@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '89871049', '7831 Dickinson Ramp Suite 138\nSouth Jerod, AR 18060', '+6043184595586', 'doctor', '9wcrza3uQ0', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (59, 'Susanna Metz', 'parker.celine@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '61837279', '91321 Lula Crest Apt. 645\nLake Lavadaborough, MD 56117-5916', '+9749389841581', 'doctor', '24Yd0Jh4dO', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (60, 'Ivory Botsford', 'rupert.jacobson@example.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '45580261', '71372 Peggie Unions Suite 732\nNew Abigale, VA 58780', '+9576869804784', 'doctor', 'ZS4MqhueSE', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (61, 'Mr. Otis Kris', 'bruen.caleigh@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '31371703', '2482 Kertzmann Drive\nBellfurt, NE 79988', '+2028790039286', 'doctor', '9fp4WOCmW8', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (62, 'Jocelyn Marks', 'malcolm27@example.org', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', '77337107', '800 Carter Ferry Apt. 928\nEast Lorainefort, CO 37255-4515', '+7520700830680', 'doctor', 'As3Qx8W3Vn', '2021-09-18 12:53:00', '2021-09-18 12:53:00', NULL);
INSERT INTO `users` VALUES (63, 'Brigham', 'info.obregon@gmail.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', NULL, 'Barranca', '930648346', 'patient', 'nbKOLzxQ0jZ2lFBOtmP4oFt5IEteAI9kbtmGcPAkSIPCoLwtLKrA8p2L2T0Q', '2021-09-18 12:58:38', '2021-09-18 13:02:52', NULL);
INSERT INTO `users` VALUES (64, 'alexis', 'alexisromero@gmail.com', NULL, '$2y$10$f4GgDn.qoVtr7Myll/n7JeKPJt6DedBYCOOIIM63MFohEjRhPmTci', NULL, NULL, NULL, 'patient', 'VJLdtuauo7U0gKYBBACkiJqxgEBmjMwfyzaP1axobWmVapuq8DwHb3VBwb73', '2021-09-18 13:01:17', '2021-09-18 13:01:17', NULL);

-- ----------------------------
-- Table structure for work_days
-- ----------------------------
DROP TABLE IF EXISTS `work_days`;
CREATE TABLE `work_days`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `day` smallint(5) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL,
  `morning_start` time(0) NOT NULL,
  `morning_end` time(0) NOT NULL,
  `afternoon_start` time(0) NOT NULL,
  `afternoon_end` time(0) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `work_days_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `work_days_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of work_days
-- ----------------------------
INSERT INTO `work_days` VALUES (1, 0, 1, '05:00:00', '11:30:00', '13:00:00', '23:30:00', 3, '2021-09-18 12:53:00', '2021-09-18 13:59:16');
INSERT INTO `work_days` VALUES (2, 1, 1, '05:00:00', '11:30:00', '13:00:00', '23:30:00', 3, '2021-09-18 12:53:00', '2021-09-18 13:59:16');
INSERT INTO `work_days` VALUES (3, 2, 1, '05:00:00', '11:30:00', '13:00:00', '23:30:00', 3, '2021-09-18 12:53:00', '2021-09-18 13:59:16');
INSERT INTO `work_days` VALUES (4, 3, 1, '05:00:00', '11:30:00', '13:00:00', '23:30:00', 3, '2021-09-18 12:53:00', '2021-09-18 13:59:16');
INSERT INTO `work_days` VALUES (5, 4, 1, '05:00:00', '11:30:00', '13:00:00', '23:30:00', 3, '2021-09-18 12:53:00', '2021-09-18 13:59:16');
INSERT INTO `work_days` VALUES (6, 5, 1, '05:00:00', '11:30:00', '13:00:00', '23:30:00', 3, '2021-09-18 12:53:00', '2021-09-18 13:59:16');
INSERT INTO `work_days` VALUES (7, 6, 1, '05:00:00', '11:30:00', '13:00:00', '23:30:00', 3, '2021-09-18 12:53:00', '2021-09-18 13:59:16');

SET FOREIGN_KEY_CHECKS = 1;
